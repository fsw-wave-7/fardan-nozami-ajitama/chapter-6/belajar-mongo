const express = require("express");
const client = require("./connection");
const router = express.Router();

// multer untuk parsing request dari form data
const multer = require("multer");

router.get("/products", async (req, res) => {
  if (client.isConnected()) {
    const db = client.db("belajar_mongo");
    const products = await db.collection("products").find().toArray();
    if (products.length > 0) {
      res.send({
        status: "Success",
        message: "List Products berhasil ditampilkan",
        data: products,
      });
    } else {
      res.send({
        status: "success",
        message: "list product kosong",
        data: products,
      });
    }
  } else {
    res.status({
      status: "error",
      message: "gagal terhubung dengan database",
    });
  }
});

const ObjectId = require("mongodb").ObjectId;
router.get("/product/:id", async (req, res) => {
  if (client.isConnected()) {
    const db = client.db("belajar_mongo");
    const product = await db.collection("products").findOne({
      _id: ObjectId(req.params.id),
    });

    res.send({
      status: "success",
      message: "Single Product",
      data: product,
    });
  } else {
    res.send({
      status: "error",
      message: "KOneksi database gagal",
    });
  }
});

// insert product
router.post("/product", multer().none(), async (req, res) => {
  if (client.isConnected()) {
    const db = client.db("belajar_mongo");
    const { name, price, stock, status } = req.body;
    const result = await db.collection("products").insertOne({
      name,
      price,
      stock,
      status,
    });
    if (result.insertedCount == 1) {
      res.send({
        status: "success",
        message: "Product berhasil ditambahkan",
      });
    } else {
      res.send({
        status: "warning",
        message: "Product gagal ditambahkan",
      });
    }
  } else {
    res.send({
      status: "error",
      message: "Koneksi database gagal",
    });
  }
});

// update product
router.put("/product/:id", multer().none(), async (req, res) => {
  if (client.isConnected()) {
    const db = client.db("belajar_mongo");
    const { name, price, stock, status } = req.body;
    const result = await db.collection("products").updateOne(
      {
        _id: ObjectId(req.params.id),
      },
      {
        $set: {
          name,
          price,
          stock,
          status,
        },
      }
    );
    if (result.matchedCount == 1) {
      res.send({
        status: "success",
        message: "Update Product berhasil",
      });
    } else {
      res.send({
        status: "warning",
        message: "Update Product gagal",
      });
    }
  } else {
    res.send({
      status: "error",
      message: "Koneksi database gagal",
    });
  }
});

// delete product
router.delete("/product/:id", async (req, res) => {
  if (client.isConnected()) {
    const db = client.db("belajar_mongo");
    const result = await db.collection("products").deleteOne({
      _id: ObjectId(req.params.id),
    });
    if (result.deletedCount == 1) {
      res.send({
        status: "success",
        message: "Delete Product berhasil",
      });
    } else {
      res.send({
        status: "WARNING!!!",
        message: "Delete Product gagal",
      });
    }
  } else {
    res.send({
      status: "error",
      message: "Koneksi database gagal",
    });
  }
});

module.exports = router;
