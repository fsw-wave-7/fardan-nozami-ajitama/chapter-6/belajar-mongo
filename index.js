const express = require("express");
const app = express();
const port = 3000;
const cors = require("cors");

// parsing raw JSON from postman
app.use(express.json());

// parsing form data from postman
app.use(express.urlencoded({ extended: false }));

const router = require("./routers");
app.use(router);
app.use(cors());

// middleware mengatasi error 404
app.use((req, res, next) => {
  res.status(404).send("halaman tidak ditemukan");
});

// Middleware menangani error 500
const errorHandling = (err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("internal Server Error");
};

app.use(errorHandling);

app.listen(port, () => {
  console.log(`server berjalan di port ${port}`);
});
